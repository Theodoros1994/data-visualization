library(coin) # # for various mean comparison tests using permutation
library(car) # Levene's and Bartlett's test

# MANN-WHITNEY
drugExperiment = read.csv("~/Rstuff/DataViz_grad/stats_datasets/drugExperiment_Wilcoxon.csv") # substitute with your local path
colnames(drugExperiment)[3:4] = c("BDI_Sun", "BDI_Wed")

wilcox_test(drugExperiment$BDI_Sun ~ drugExperiment$Drug, distribution = 'exact', conf.int = T) # Not significant
wilcox_test(drugExperiment$BDI_Wed ~ drugExperiment$Drug, distribution = 'exact', conf.int = T) # Significant


# WILCOXON
ecstasyData = subset(drugExperiment, subset = drugExperiment$Drug == 'Ecstasy')
wilcoxsign_test(ecstasyData$BDI_Wed ~ ecstasyData$BDI_Sun, distribution = 'exact')
wilcox.test(ecstasyData$BDI_Wed, ecstasyData$BDI_Sun, paired = T)

alcoholData = subset(drugExperiment, subset = drugExperiment$Drug == 'Alcohol')
wilcoxsign_test(alcoholData$BDI_Wed ~ alcoholData$BDI_Sun, distribution = 'exact')

# KRUSKAL-WALLIS
soya = read.csv("~/Rstuff/DataViz_grad/stats_datasets/soyaSpermCount_Kruskal.csv")
soya$X = NULL
soya$soyaMeals = factor(soya$soyaMeals, ordered = T, levels = c("None", "One", "Four", "Seven"))

soya$ranks = rank(soya$spermCount) # Add a 'ranks' column to the entire dataset
by(soya$ranks, soya$soyaMeals, mean) # Present summary info on 'ranks' by 'soyaMeans', output = mean (=> MEAN RANKS)

library(pgirmess) # for multiple comparisons (next line)
kruskalmc(soya$spermCount ~ soya$soyaMeals)
pairwise.wilcox.test(soya$spermCount, soya$soyaMeals, paired = F) # More convenient comparison function



## WE HAVEN'T SEEN THESE IN CLASS

# Jonckheere test to detect trends
library(clinfun)
jonckheere.test(soya$spermCount, soya$soyaMeals)


# FRIEDMMAN
friedman_test(as.matrix(weights))
library(PMCMR) # for various post-hoc tests
posthoc.friedman.conover.test(as.matrix(weights), p.adjust = "holm")
