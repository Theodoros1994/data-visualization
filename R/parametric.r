# t-test, UNRELATED
spidersU = read.csv("~/Rstuff/DataViz_grad/stats_datasets/arachnophobiaIndependent.csv")
leveneTest(spidersU$Anxiety ~ spidersU$Group) # Levene test for homogeneity of variances
t.test(spidersU$Anxiety ~ spidersU$Group, var.equal = T)

# t-test, RELATED
spidersD = read.csv("~/Rstuff/DataViz_grad/stats_datasets/arachnophobiaDependent.csv")
t.test(spidersD$anxietyPicture, spidersD$anxietyReal, paired = T)

# Robust mean comparison -- WE DIDN'T COVER THIS IN CLASS

library(WRS2)
pb2gen(spidersU$Anxiety ~ spidersU$Group, nboot = 1000) # for independent groups
bootdpci


# Effect size for 2 groups
library(effsize)
cohen.d.formula(spidersU$Anxiety ~ spidersU$Group, pooled = T, hedges.correction = T, paired = F)


# ANALYSIS OF VARIANCE

library(compute.es)
library(car)
library(ggplot2)
library(multcomp)
library(pastecs)
library(WRS2)

# Viagra data
libido = c(3,2,1,1,4,5,2,4,2,3,7,4,5,3,6)
dose = gl(3,5, labels = c("Placebo", "Low Dose", "High Dose"))
viagraData = data.frame(dose, libido)

leveneTest(viagraData$libido, viagraData$dose)

vModel = aov(libido ~ dose, data = viagraData)
summary(vModel)
plot(vModel)

## NOT COVERED IN CLASS BELOW THIS POINT

oneway.test(viagraData$libido ~ viagraData$dose, data = viagraData) # Welch ANOVA


# Robust ANOVA
t1waybt(viagraData$libido ~ viagraData$dose, tr = .05, nboot = 2000) # Trimmed mean, 2000 random samples

## NOT COVERED IN CLASS UP TO THIS POINT


# Post-hoc: Bonferroni-related methods
pairwise.t.test(viagraData$libido, viagraData$dose, paired = F, p.adjust.method = 'bonferroni')
pairwise.t.test(viagraData$libido, viagraData$dose, paired = F, p.adjust.method = 'BH')

# Post-hoc: Tukey and Dunnett
library(multcomp)
postHoc = glht(vModel, mcp(dose = "Tukey"))
summary(postHoc)
confint(postHoc)
plot(postHoc)


## NOT COVERED IN CLASS BELOW THIS POINT (TO END OF FILE)

# ANOVA effect sizes (check: https://cran.r-project.org/web/packages/sjstats/vignettes/anova-statistics.html)

# Eta-squared
library(sjstats)
eta_sq(vModel)
omega_sq(vModel) # ω^2 (omega-square): .01, .06 and .14 represent small, medium and large effects respectively (Kirk, 1996)
anova_stats(vModel) # complete table

library(compute.es)


# REPEATED MEASURES ANOVA
library(reshape)
longBush = melt(bushtucker, id = "Celebrity", measured = c("Stick_insect", "Kangaroo_testicle", "Fish_eye", "Witchetty_grub"))
names(longBush) = c("Participant", "Animal", "Retch")
longBush$Participant = as.factor(longBush$Participant)

bushModel = ezANOVA(data = longBush, dv = .(Retch), wid = .(Participant),  within = .(Animal), detailed = TRUE, type = 3)

# Pairwise comparisons for repeated measures
pairwise.t.test(longBush$Retch, longBush$Animal, paired = TRUE, p.adjust.method = "bonferroni")
