library(plotly)
library(ggplot2)
library(dplyr)
library(tidyr)
install.packages("DT")
library(DT)
library(maps)
library(choroplethr)
library(choroplethrAdmin1)
library(ggplot2)
library(plyr)
arrange(dd,desc(z),b)
library(rworldmap)
install.packages("magrittr")
library(magrittr)
library(highcharter)
library(dplyr)
library(tidyr)
df <- read.csv(file="C:/Users/teo/Downloads/GlobalFirePower_multiindex.csv", header=TRUE)
df_sorted<-  df[order(df$Rank),]
#df = df[-1,]
df_sorted10<-subset(df_sorted,Rank<11)
df_sorted10[c("Army","Navy","Airforce")]<-NA
df_melted=melt(df_sorted10, id.vars = "Navy", measure.vars = c("Submarines", "Frigates"))

sub_army<-subset(df_sorted10, select=c("Country", "Rocket.Projectors","Combat.Tanks","Towed.Artillery"))
sub_navy<-subset(df_sorted10, select=c("Country", "Corvettes","Frigates","Submarines","Destroyers","Aircraft.Carriers"))
sub_air<-subset(df_sorted10, select=c("Country", "Fighter.Aircraft","Attack.Aircraft","Transport.Aircraft"))

#army plot
mm <- melt(sub_army, id='Country')
p <- ggplot(data = mm, aes(x = variable, y = value, group = variable, fill = variable))
p <- p + geom_bar(stat = "identity", width = 0.5, position = "dodge")
p <- p + facet_grid(. ~ Country)
p <- p + theme_bw()
p <- p + theme(axis.text.x = element_text(angle = 90))
p
#navy plot
mm <- melt(sub_navy, id='Country')
p <- ggplot(data = mm, aes(x = variable, y = value, group = variable, fill = variable))
p <- p + geom_bar(stat = "identity", width = 0.5, position = "dodge")
p <- p + facet_grid(. ~ Country)
p <- p + theme_bw()
p <- p + theme(axis.text.x = element_text(angle = 90))
p

#airforce plot
mm <- melt(sub_air, id='Country')
p <- ggplot(data = mm, aes(x = variable, y = value, group = variable, fill = variable))
p <- p + geom_bar(stat = "identity", width = 0.5, position = "dodge")
p <- p + facet_grid(. ~ Country)
p <- p + theme_bw()
p <- p + theme(axis.text.x = element_text(angle = 90))
p



#barplot with black letters
ggplot(data=df_sorted10, aes(x=Country, y=Major.Ports...Terminals)) +
  geom_bar(stat="identity", fill="steelblue")+
  geom_text(aes(label=Major.Ports...Terminals), vjust=-0.8, color="black", size=3.5)+
  theme(axis.text.x=element_text(angle=90,hjust=1,vjust=0.5))

 
# barplot 
GlobalFirePower_TopTenMilitary <- df_sorted10 %>% select(Country, Active.Personnel) %>% arrange(desc(Active.Personnel))
hchart(GlobalFirePower_TopTenMilitary, "column", hcaes(x = Country, y = Active.Personnel), color = "green", name = "Active Army") %>%
  hc_title(text = "Active Military Strength") %>%
  hc_add_theme(hc_theme_google()) %>%
  hc_credits(enabled = TRUE, text = "Active Personnel Plot", style = list(fontSize = "10px"))

#treemap
hchart(df_sorted10, "treemap", hcaes(x = Country, value = Fighter.Aircraft, color = Fighter.Aircraft)) %>%
  hc_title(text = "Fighter Aircraft Strength") %>%
  hc_add_theme(hc_theme_google()) %>%
  hc_credits(enabled = TRUE, text = "Aircraft Treemap", style = list(fontSize = "10px"))

#barplot2
hc <- highchart() %>% 
  hc_xAxis(categories = df_sorted10$Country) %>% 
  hc_title(text = "<b>Total, Active, and Reserve Military Strength of top 10 superpowers</b>") %>%
  hc_add_series(name = "Total Military Personnel", data = df_sorted10$Total.Military.Personnel) %>% 
  hc_add_series(name = "Active Personnel", data = df_sorted10$Active.Personnel) %>% 
  hc_add_series(name = "Reserve Personnel",
                data = df_sorted10$Reserve.Personnel) %>%
  hc_credits(enabled = TRUE, text = "", style = list(fontSize = "10px"))

hc <- hc %>% 
  hc_chart(type = "column",
           options3d = list(enabled = TRUE, beta = 15, alpha = 15))
hc
#line graph
hc <- highchart() %>% 
  hc_xAxis(categories = df_sorted10$Country) %>% 
  hc_title(text = "Aircraft Strength of top 10 superpowers") %>%
  hc_add_series(name = "Total Aircraft Strength", data = df_sorted10$Total.Aircraft.Strength) %>% 
  hc_add_series(name = "Fighter Craft", data = df_sorted10$Fighter.Aircraft) %>%
  hc_add_series(name = "Attack Aircraft",
                data = df_sorted10$Attack.Aircraft) %>%
  hc_add_series(name = "Transport Aircraft",
                data = df_sorted10$Transport.Aircraft) %>%
  hc_add_series(name = "Trainer Aircraft",
                data = df_sorted10$Trainer.Aircraft) %>%
  hc_credits(enabled = TRUE, text = "", style = list(fontSize = "10px")) %>%
  hc_tooltip(crosshairs = TRUE, backgroundColor = "#FCFFC5",
             shared = TRUE, borderWidth = 5)
hc
##piechart+bartplot
hc <- highchart() %>% 
  hc_title(text = "Combat Tanks and Fighting Vehicles Strength of top 10 superpowers") %>%
  hc_add_series(df_sorted10, "pie", hcaes(name = as.character(Country), y = as.numeric(Combat.Tanks)), name = "Combat Tanks Strength") %>%
  hc_add_series(df_sorted10, "column", hcaes(x = as.character(Country), y = as.numeric(Armored.Fighting.Vehicles)), name = "Armored Fighting Vehicles Strength") %>%
  hc_plotOptions(
    series = list(
      showInLegend = FALSE,
      pointFormat = "{point.y}%"
    ),
    column = list(
      colorByPoint = TRUE
    ),
    pie = list(
      colorByPoint = TRUE, center = c('30%', '10%'),
      size = 120, dataLabels = list(enabled = FALSE)
    )) %>% hc_xAxis(categories = df_sorted10$Country) %>%
  hc_credits(enabled = TRUE, text = "", style = list(fontSize = "10px"))
hc
##horizontal barplot
hc_opts <- list()
hc_opts$chart <- list(type = "bar")
hc_opts$title <- list(title = "Oil Production and Consumption (bbl/day)")
hc_opts$xAxis <- list(categories = df_sorted10$Country)
hc_opts$yAxis <- list(title = list('bbl/day'))
hc_opts$legend <- list(reversed = TRUE)
hc_opts$series <- list(list(name = "Production (bbl/day)", data =df_sorted10$Production..bbl.dy.),
                       list(name = "Consumption (bbl/day)", data = df_sorted10$Consumption..bbl.dy.))

highchart(hc_opts, theme = hc_theme_darkunica())
##line graph 2
hc <- highchart() %>% 
  hc_xAxis(categories = df_sorted10$Country) %>% 
  hc_title(text = "Defence budget of top 10 superpowers") %>%
  hc_add_series(name = "Defence budget", data = df_sorted10$Defense.Budget) %>%  hc_credits(enabled = TRUE, text = "", style = list(fontSize = "10px")) %>%  hc_add_theme(hc_theme_handdrawn())
hc
##

# barchart 3
hc <- highchart() %>% 
  hc_xAxis(categories = df_sorted10$Country) %>% 
  hc_title(text = "External debt of top 10 superpowers") %>%
  hc_add_series(name = "External Debt", data = df_sorted10$External.Debt) %>%  hc_credits(enabled = TRUE, text = "", style = list(fontSize = "10px")) %>%  hc_chart(type = "waterfall")
hc

#barplot with numbers
ggplot(data=df_sorted10, aes(x=Country, y=Total.Military.Personnel)) +
  geom_bar(stat="identity", fill="steelblue")+
  geom_text(aes(label=Active.Personnel), vjust=0.8, color="white", size=3.5)+
  theme(axis.text.x=element_text(angle=90,hjust=1,vjust=0.5))





#~~~~~~~~~~~~~~~~~~~~~~~~~~~~
dev.off()



library("ggplot2")






library(RColorBrewer)
#Debt Map
countries.met<-data.frame(df_sorted10$Country,df_sorted10$External.Debt)
#countries.met<-data.frame(MyData$Country.Data,MyData$Finance)
colnames(countries.met) <- c("country", "value")
matched <- joinCountryData2Map(countries.met, joinCode="NAME", nameJoinColumn="country")
mapCountryData(matched, nameColumnToPlot="value", mapTitle="Top 10 Military Countries Debt Map 2017", catMethod = "pretty", colourPalette = "heat")




# square land map
countries.met<-data.frame(df_sorted10$Country,df_sorted10$Square.Land.Area..km.)
#countries.met<-data.frame(MyData$Country.Data,MyData$Finance)
colnames(countries.met) <- c("country", "value")
matched <- joinCountryData2Map(countries.met, joinCode="NAME", nameJoinColumn="country")

mapCountryData(matched, nameColumnToPlot="value", mapTitle="Top 10 Military Countries Square Land Map",  catMethod = "pretty",  colourPalette = "terrain")




#map
library("reshape2")

#map purple oil 
countries.met<-data.frame(df_sorted10$Country,df_sorted10$Proven.Reserves..bbl.dy.)
#countries.met<-data.frame(MyData$Country.Data,MyData$Finance)
colnames(countries.met) <- c("country", "value")
matched <- joinCountryData2Map(countries.met, joinCode="NAME", nameJoinColumn="country")

mapCountryData(matched, nameColumnToPlot="value", mapTitle="Oil Reverses Map", 
               catMethod = "pretty",  colourPalette = "white2Black")




library(classInt)
library(RColorBrewer)
#map purple oil 
countries.met<-data.frame(df_sorted10$Country,df_sorted10$Defense.Budget)
#countries.met<-data.frame(MyData$Country.Data,MyData$Finance)
colnames(countries.met) <- c("country", "value")
matched2 <- joinCountryData2Map(countries.met, joinCode="NAME", nameJoinColumn="country",mapResolution = "coarse")

#RENAME MATCHED2 VALUE
#getting class intervals
classInt <- classIntervals( matched2[["value"]]
                            ,n=5, style = "jenks")
catMethod = classInt[["brks"]]
#getting colours
colourPalette <- brewer.pal(5,'RdPu')
#plot map
mapDevice() #create world map shaped window
mapParams <- mapCountryData(matched2
                            ,nameColumnToPlot="value"
                            ,addLegend=FALSE
                            ,catMethod = catMethod, mapTitle="Defense Budget Map"
                            ,colourPalette=colourPalette )
#adding legend
do.call(addMapLegend
        ,c(mapParams
           ,legendLabels="all"
           ,legendWidth=0.5
           ,legendIntervals="data"
           ,legendMar = 2))





library(plotly)
#Map 1
# give state boun
l <- list(color = toRGB("black"), width = 2)
# specify some map projection/options
g <- list(
  
  projection = list(type = 'Mercator'),
  showlakes = TRUE,
  lakecolor = toRGB('white')
)
plot_ly(df_sorted10, z =~Total.Aircraft.Strength, text = ~Country, locations = ~ISO3, type = 'choropleth',
        locationmode = 'world', color = ~Total.Aircraft.Strength, colors = 'YlGnBu',
        marker = list(line = l), colorbar = list(title = "Air Units"),
        filename="r-docs/usa-choropleth") %>%
  layout(title = 'Total Air Strength  Map', geo = g)

#Map 2 

plot_ly(df_sorted10, z =~Total.Naval.Assets, text = ~Country, locations = ~ISO3, type = 'choropleth',
        locationmode = 'world', color = ~Total.Naval.Assets, colors = 'Blues',
        marker = list(line = l), colorbar = list(title = "Navy Units"),
        filename="r-docs/usa-choropleth") %>%
  layout(title = 'Total Navy Assets Map', geo = g)


#Map 3
plot_ly(df_sorted10, z =~Armored.Fighting.Vehicles, text = ~Country, locations = ~ISO3, type = 'choropleth',
        locationmode = 'world', color = ~Armored.Fighting.Vehicles, colors = 'Greens',
        marker = list(line = l), colorbar = list(title = "Army Units"),
        filename="r-docs/usa-choropleth") %>%
  layout(title = 'Total Army Units Map', geo = g)

#Map 3
plot_ly(df_sorted10, z =~Merchant.Marine.Strength, text = ~Country, locations = ~ISO3, type = 'choropleth',
        locationmode = 'world', color = ~Merchant.Marine.Strength, colors = 'RdBu',
        marker = list(line = l), colorbar = list(title = "Merchant Marines"),
        filename="r-docs/usa-choropleth") %>%
  layout(title = 'Merchant Marine Map', geo = g)

plot_ly(df_sorted10, z =~External.Debt, text = ~Country, locations = ~ISO3, type = 'choropleth',
        locationmode = 'world', color = ~External.Debt, colors = 'Reds',
        marker = list(line = l), colorbar = list(title = "Debt"),
        filename="r-docs/usa-choropleth") %>%
  layout(title = 'External Debt Map', geo = g)


